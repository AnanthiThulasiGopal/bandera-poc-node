import { Api } from "../decorators/api.decorators";
import { UserService } from "../service/users.service";
import { Logger } from "../utils/logger.utils";
import { BusinessException } from "../exceptions/business.exception";
import ResponseUtils from '../utils/response.utils';
import { STATUS } from '../constants/status.constants';

@Api
export class UsersApi {

    static create(event, context, cb) {
        return new Promise(async (resolve, reject) => {
            try {
                console.log(`Entering <UsersApi.create> ${JSON.stringify(event.body)}`);
                let body = JSON.parse(event.body);
                Logger.info(body);
                if (!Object.keys(body).length) {
                    throw new BusinessException('User data cannot be empty');
                }
                const response = await UserService.createUser(body);
                Logger.info(`Resolving promise from <UsersApi.create> with ${JSON.stringify(response)}`);
                resolve(ResponseUtils.generateResponse(STATUS.SUCCESS, response))
            } catch (error) {
                Logger.error(error);
                Logger.info('Rejecting promise from <UsersApi.create>');
                resolve(ResponseUtils.generateResponse(STATUS.ERROR, error));
            }
        });
    }

    static getAllUsers(event, context, cb) {
        return new Promise(async (resolve, reject) => {
            try {
                Logger.info(`Entering <UsersApi.getAllUsers>`);
                const result = await UserService.getAllUsers();
                if(!result){
                    throw new BusinessException('No users found')
                }
                Logger.info(`Resolving promise from <UsersApi.getAllUsers> with ${JSON.stringify(result)}`);
                resolve(ResponseUtils.generateResponse(STATUS.SUCCESS, result))
            } catch (error) {
                Logger.error(error);
                Logger.info('Rejecting promise from <UsersApi.getAllUsers>');
                resolve(ResponseUtils.generateResponse(STATUS.ERROR, error));
            }
        });
    }

    static getUserById(event, context, cb) {
        return new Promise(async (resolve, reject) => {
            try {
                Logger.info('Entering <UsersApi.getUserById> with', event);
                Logger.info(event.pathParameters);
                const result = await UserService.getUserById(event.pathParameters.id);
                if (!result) {
                    throw new BusinessException('User not found')
                }
                Logger.info(`Resolving promise from <UsersApi.getUserById> with ${JSON.stringify(result)}`);
                resolve(ResponseUtils.generateResponse(STATUS.SUCCESS, result))
            } catch (error) {
                Logger.error(error);
                Logger.info('Rejecting promise from <UsersApi.getUserById>');
                resolve(ResponseUtils.generateResponse(STATUS.ERROR, error));
            }
        });
    }

    static deleteUser(event, context, cb) {
        return new Promise(async (resolve, reject) => {
            try {
                Logger.info('Entering <UsersApi.deleteUserById> with', event.pathParameters.id);
                const result = await UserService.deleteUser(event.pathParameters.id);
                Logger.info(`Resolving promise from <UsersApi.deleteUser> with ${JSON.stringify(result)}`);
                resolve(ResponseUtils.generateResponse(STATUS.SUCCESS, result))
            } catch (error) {
                Logger.error(error);
                Logger.info('Rejecting promise from <UsersApi.deleteUserById>');
                resolve(ResponseUtils.generateResponse(STATUS.ERROR, error));
            }
        });
    }

    static updateUser(event, context, cb) {
        return new Promise(async (resolve, reject) => {
            try {
                Logger.info(`Entering <UsersApi.updateUser> with ${JSON.stringify(event)}`);
                let body = JSON.parse(event.body)
                if (!Object.keys(body).length) {
                    throw new BusinessException('User data cannot be empty');
                }
                const response = await UserService.updateUser(body);
                Logger.info(`Resolving promise from <UsersApi.deleteUser> with ${JSON.stringify(response)}`);
                resolve(ResponseUtils.generateResponse(STATUS.SUCCESS, response))
            } catch (error) {
                Logger.error(error);
                Logger.info('Rejecting promise from <UsersApi.updateUser>');
                resolve(ResponseUtils.generateResponse(STATUS.ERROR, error));
            }
        });
    }
}

export const create = UsersApi.create;
export const getAllUsers = UsersApi.getAllUsers;
export const getUserById = UsersApi.getUserById;
export const deleteUser = UsersApi.deleteUser;
export const updateUser = UsersApi.updateUser;