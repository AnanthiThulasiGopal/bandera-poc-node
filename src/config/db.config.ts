import { Logger } from '../utils/logger.utils';
import { User, UserTypes } from "../dao/models/user.model"
import { Sequelize } from 'sequelize-typescript';
import { Session } from '../namespaces/session.namespace';

export class DbConfig {
    private sequelize: Sequelize;
    constructor() {
        const host = process.env['DB_HOST']
            , database = process.env['DATABASE']
            , username = process.env['DB_USERNAME']
            , password = process.env['DB_PASSWORD']
        this.sequelize = new Sequelize(database, username, password, {
            host: host,
            dialect: 'mysql',
            port: 3306, 
            dialectOptions: {
                connectTimeout: 25000
            },
            pool: {
                max: 10,
                min: 0,
                acquire: 10000,
                idle: 10000
            },
            benchmark: true
          });

        this.init();
        this.sequelize.authenticate();
    }

    init() {
        Logger.debug('Entering <init>')
        this.registerModels();
        Session.setValue('SEQUELIZE', this.sequelize);
        Logger.debug('Exiting <init>')
    }

    registerModels() {
        Logger.debug('Entering <registerModels>');
        this.sequelize.addModels([
            User,
            UserTypes
        ]);
        Logger.debug('Exiting <registerModels>')
    }
}