import { Logger } from "../utils/logger.utils";
import { User, UserTypes } from "../dao/models/user.model";
import { getTransaction } from "../utils/common.utils";
import { BusinessException } from "../exceptions/business.exception";
import { IUpdateReq, ICreateUserReq } from '../beans/requests.bean';

export class UserService {
    public static createUser(userParams: ICreateUserReq) {
        return new Promise(async (resolve, reject) => {
            const trans = await getTransaction();
            try {
                Logger.info(`Entering <UserService.createUser> with ${JSON.stringify(userParams)}`);
                const user: User = await User.create(userParams, {
                    transaction: trans
                });
                if (!user) {
                    throw new BusinessException('Error while creating user')
                }
                await trans.commit();
                const createdUser = await this.getUserById(user.id);
                Logger.info('Resolving promise of <UserService.createUser>', createdUser);
                resolve(createdUser);
            } catch (err) {
                Logger.error(err);
                Logger.info('Error Resolving Query');
                await trans.rollback();
                Logger.info('Rejecting promise of <UserService.createUser>');
                reject(err);
            }
        })
    }
    public static getAllUsers() {
        return new Promise(async (resolve, reject) => {
            try {
                Logger.info('Entering <UserService.getAllUsers>');
                const result: any = await User.findAndCountAll({
                    include: [{
                        model: UserTypes,
                        attributes: ['id', 'type']
                    }],
                    order: [['createdAt', 'DESC']],
                    where: { isDeleted: false }
                });
                Logger.info(`Resolving promise of <UserService.getAllUsers>with ${JSON.stringify(result)}`);
                resolve(result);
            } catch (err) {
                Logger.error(err);
                Logger.info('Error Resolving Query');
                Logger.info('Rejecting promise of <UserService.getAllUsers>');
                reject(err);
            }
        })
    }
    public static getUserById(id: number) {
        return new Promise(async (resolve, reject) => {
            try {
                Logger.info('Entering <UserService.getUserById> with id: ', id);
                let result: any;
                result = await User.findOne({
                    include: [{
                        model: UserTypes,
                        attributes: ['id', 'type']
                    }],
                    order: [['createdAt', 'DESC']],
                    where: { id: id, isDeleted: false }
                });
                Logger.info('Resolving promise of <UserService.getUserById>', result);
                resolve(result);
            } catch (err) {
                Logger.error(err);
                Logger.info('Error Resolving Query');
                Logger.info('Rejecting promise of <UserService.getUserById>');
                reject(err);
            }
        })
    }
    public static updateUser(userParams: IUpdateReq) {
        return new Promise(async (resolve, reject) => {
            const trans = await getTransaction();
            try {
                Logger.info(`Entering <UserService.updateUser> with ${JSON.stringify(userParams)}`);
                const result = await User.findOne({
                    where: { id: userParams['id'], isDeleted: false }
                });
                if (!result) {
                    throw new BusinessException('User not found');
                }
                await User.update(
                    { userName: userParams['userName'], phoneNumber: userParams['phoneNumber'], address: userParams['address'], employeeId: userParams['employeeId'], userTypeId: userParams['userTypeId'] },
                    {
                        where: { id: userParams['id'] },
                        transaction: trans
                    }
                );
                await trans.commit();
                const updatedUser = await this.getUserById(userParams['id']);
                Logger.info('Resolving promise of <UserService.updateUser>', updatedUser);
                resolve(updatedUser);
            } catch (err) {
                Logger.error(err);
                Logger.info('Error Resolving Query');
                await trans.rollback();
                Logger.info('Rejecting promise of <UserService.updateUser>');
                reject(err);
            }
        })
    }
    public static deleteUser(userId: number) {
        return new Promise(async (resolve, reject) => {
            const trans = await getTransaction();
            try {
                await User.update(
                    { isDeleted: '1' },
                    {
                        where: { id: userId },
                        transaction: trans
                    }
                )
                await trans.commit();
                Logger.info(`Resolving promise of <UserService.deleteUser>`);
                resolve("User deleted successfully");
            } catch (err) {
                Logger.error(err);
                Logger.info('Error Resolving Query');
                await trans.rollback();
                Logger.info('Rejecting promise of <UserService.deleteUser>');
                reject(err);
            }
        })
    }
}