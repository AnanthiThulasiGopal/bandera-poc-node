import { Model, PrimaryKey, AutoIncrement, DataType, Column, ForeignKey, Table, BelongsTo } from "sequelize-typescript";

@Table({
    tableName: 'userTypes',
    underscored: true
})
export class UserTypes extends Model<UserTypes> {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    id: number;

    @Column(DataType.STRING)
    type: string;
}

@Table({
    tableName: 'users'
})
export class User extends Model<User> {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    id: number;

    @Column(DataType.STRING)
    userName: string;

    @Column(DataType.STRING)
    phoneNumber: string;

    @Column(DataType.STRING)
    address: string;

    @Column(DataType.INTEGER)
    employeeId: number;

    @Column(DataType.BOOLEAN)
    isDeleted: boolean;

    @Column(DataType.DATE)
    createdAt?: Date;

    @Column(DataType.DATE)
    updatedAt?: Date;

    @ForeignKey(() => UserTypes)
    @Column(DataType.INTEGER)
    userTypeId: number;

    @BelongsTo(() => UserTypes, 'userTypeId')
    userType: UserTypes
}
