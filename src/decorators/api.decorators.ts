import { Logger } from "../utils/logger.utils";
import { DbConfig } from "../config/db.config";

export const Api = (constructor: Function) => {
    Logger.debug('Sealing Object');
    Object.seal(constructor);
    new DbConfig();
}