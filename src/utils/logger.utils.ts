const pino = require('pino');

const logger = pino({
  level: 'trace',
  base: null,
  prettyPrint: {
    translateTime: 'yyyy-mm-dd HH:MM:ss.l Z',
    colorize: true,
    levelFirst: true
  }
});

export const Logger = logger;
