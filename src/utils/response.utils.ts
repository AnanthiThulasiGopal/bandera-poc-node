import { STATUS } from '../constants/status.constants';

export default class ResponseUtils {
    public static generateResponse(statusCode, responseData) {
        switch (statusCode) {
            case STATUS.ERROR:
                return {
                    statusCode: STATUS.ERROR,
                    body: JSON.stringify({
                        error: {
                            message: responseData.message
                        }
                    })
                }
            case STATUS.SUCCESS: {
                return {
                    statusCode: STATUS.SUCCESS,
                    body: JSON.stringify(responseData)
                };
            }

        }
    }
}