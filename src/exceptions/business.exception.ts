import { IError } from "../beans/common.bean";

export class BaseError extends Error {

    constructor(error: IError) {
        super(`${error.message}`);
    }

}

export class BusinessException extends BaseError {
    constructor(err: string) {
        const error: IError = {
            message: err,
            statusCode: 400,
            errorType: 'BusinessException'
        };
        super(error);
    }
}
