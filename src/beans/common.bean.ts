
export interface IError {
    message: string;
    statusCode: number;
    errorType: string;
}