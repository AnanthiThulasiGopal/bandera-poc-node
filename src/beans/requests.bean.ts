export interface IUpdateReq {
    id: number
    phoneNumber: string;
    userName: string;
    employeeId: number;
    userTypeId: number;
    address: string;
}
export interface ICreateUserReq{
    phoneNumber: string;
    userName: string;
    employeeId: number;
    userTypeId: number;
    address: string;
}