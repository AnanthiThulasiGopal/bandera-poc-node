import { IUpdateReq, ICreateUserReq } from '../../src/beans/requests.bean';

export const stubValue: IUpdateReq = {
    id: 1,
    userName: 'Ananthi',
    phoneNumber: '8220074438',
    address: 'Madurai',
    employeeId: 100,
    userTypeId: 1
};

export const getAllUsersStub = {
    count: 2,
    rows: [
        {
            id: 1,
            userName: 'Ananthi',
            phoneNumber: '8220074438',
            address: 'Madurai',
            employeeId: 100,
            isDeleted: false,
            createdAt: '2020-05-15T05:27:25.000Z',
            updatedAt: '2020-05-15T05:40:41.000Z',
            userTypeId: 1,
            userType: {
                id: 1,
                type: 'admin'
            }
        },
        {
            id: 2,
            userName: 'Sapna',
            phoneNumber: '8220074438',
            address: 'Madurai',
            employeeId: 100,
            isDeleted: false,
            createdAt: '2020-05-15T05:27:25.000Z',
            updatedAt: '2020-05-15T05:40:41.000Z',
            userTypeId: 1,
            userType: {
                id: 1,
                type: 'admin'
            }
        }
    ]
}

export const createUserStub: ICreateUserReq = {
    userName: 'Joseph',
    phoneNumber: '9034567890',
    address: 'Thiruchendur',
    employeeId: 130,
    userTypeId: 3
};
