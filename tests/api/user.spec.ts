import * as chai from 'chai';
import * as sinon from 'sinon';
import { UsersApi } from '../../src/api/user';
import { UserService } from '../../src/service/users.service';
import { IUpdateReq } from '../../src/beans/requests.bean';
import * as chaiAsPromised from 'chai-as-promised';
chai.use(chaiAsPromised);
const event = { 'pathParameters': { 'id': 100 } }
const stubValue: IUpdateReq = {
    id: 1,
    userName: 'Ananthi',
    phoneNumber: '8220074438',
    address: 'Madurai',
    employeeId: 100,
    userTypeId: 3,
};

const createEvent = {
    body: JSON.stringify({
        userName: 'Ananthi',
        phoneNumber: '8220074498',
        address: 'Madurai',
        employeeId: 100,
        userTypeId: 3
    })
}

const updateEvent = {
    body: JSON.stringify({
        id: 1,
        userName: 'Naveen',
        phoneNumber: '8438472753',
        address: 'Chennai',
        employeeId: 100,
        userTypeId: 3
    })
}
const errorEvent = {
    body: JSON.stringify({
        
    })
}

describe("Users Api", function () {
    afterEach(function () {
        sinon.restore();
    });
    describe("getUserById api", function () {
        it("should retrieve a user with specific id", async function () {
            const stub = sinon.stub(UserService, "getUserById").returns(stubValue);
            const userData: any = await UsersApi.getUserById(event, null, null)
            const user: any = JSON.parse(userData.body);
            chai.expect(stub.calledOnce).to.be.true;
            chai.expect(user.id).to.equal(stubValue.id);
            chai.expect(user.userName).to.equal(stubValue.userName);
            chai.expect(user.phoneNumber).to.equal(stubValue.phoneNumber);
            chai.expect(user.address).to.equal(stubValue.address);
            chai.expect(user.employeeId).to.equal(stubValue.employeeId);
            chai.expect(user.userTypeId).to.equal(stubValue.userTypeId);
        });
        it("should send error", async function () {
            const stub = sinon.stub(UserService, "getUserById").returns(null);
            const res: any = await UsersApi.getUserById(event, null, null)
            const responseCode = JSON.parse(res.statusCode);
            const responseBody = JSON.parse(res.body);
            chai.expect(responseCode).to.equal(400);
            chai.expect(responseBody.error.message).to.equal('User not found');
            chai.expect(stub.calledOnce).to.be.true;
        });
    });
    describe("createUser api", function () {
        it("should create a user with specific id", async function () {
            const stub = sinon.stub(UserService, "createUser").returns(stubValue);
            const userData: any = await UsersApi.create(createEvent, null, null)
            const user: any = JSON.parse(userData.body);
            chai.expect(stub.calledOnce).to.be.true;
            chai.expect(user.id).to.equal(stubValue.id);
            chai.expect(user.userName).to.equal(stubValue.userName);
            chai.expect(user.phoneNumber).to.equal(stubValue.phoneNumber);
            chai.expect(user.address).to.equal(stubValue.address);
            chai.expect(user.employeeId).to.equal(stubValue.employeeId);
            chai.expect(user.userTypeId).to.equal(stubValue.userTypeId);
        });

        it("should send error while creating with invalid data", async function () {
            const res: any = await UsersApi.create(errorEvent, null, null)
            const responseCode = JSON.parse(res.statusCode);
            const responseBody = JSON.parse(res.body);
            chai.expect(responseCode).to.equal(400);
            chai.expect(responseBody.error.message).to.equal('User data cannot be empty');
        });
    });
    describe("updateUser api", function () {
        it("should update a user", async function () {
            const stub = sinon.stub(UserService, "updateUser").returns(stubValue);
            const userData: any = await UsersApi.updateUser(updateEvent, null, null)
            const user: any = JSON.parse(userData.body);
            chai.expect(stub.calledOnce).to.be.true;
            chai.expect(user.id).to.equal(stubValue.id);
            chai.expect(user.userName).to.equal(stubValue.userName);
            chai.expect(user.phoneNumber).to.equal(stubValue.phoneNumber);
            chai.expect(user.address).to.equal(stubValue.address);
            chai.expect(user.employeeId).to.equal(stubValue.employeeId);
            chai.expect(user.userTypeId).to.equal(stubValue.userTypeId);

            stub.reset();
        });

        it("should send error while updating with invalid data", async function () {
            const res: any = await UsersApi.updateUser(errorEvent, null, null)
            const responseCode = JSON.parse(res.statusCode);
            const responseBody = JSON.parse(res.body);
            chai.expect(responseCode).to.equal(400);
            chai.expect(responseBody.error.message).to.equal('User data cannot be empty');
        });
    });
});