import * as chai from 'chai';
import * as sinon from 'sinon';
import { User } from '../../src/dao/models/user.model';
import { UserService } from '../../src/service/users.service';
import * as common from  '../../src/utils/common.utils';
import * as chaiAsPromised from 'chai-as-promised';
chai.use(chaiAsPromised);
import { stubValue, getAllUsersStub, createUserStub } from '../mock/user-mock'

describe("UsersService", function () {

    afterEach(function () {
        sinon.restore();
    });
    describe("getUserById", function () {

        it("should retrieve a user with specific id", async function () {
            const stub = sinon.stub(User, "findOne").returns(stubValue);
            const user: any = await UserService.getUserById(stubValue.id)

            chai.expect(stub.calledOnce).to.be.true;
            chai.expect(user.id).to.equal(stubValue.id);
            chai.expect(user.userName).to.equal(stubValue.userName);
            chai.expect(user.phoneNumber).to.equal(stubValue.phoneNumber);
            chai.expect(user.address).to.equal(stubValue.address);
            chai.expect(user.employeeId).to.equal(stubValue.employeeId);
            chai.expect(user.userTypeId).to.equal(stubValue.userTypeId);

            stub.reset();
        });
    });

    describe("updateUser service", function () {
        it("should throw error when invalid id is passed", async function () {
            const stub = sinon.stub(User, "update").returns(stubValue)
            const user: any = await UserService.updateUser(stubValue)

            chai.expect(stub.calledOnce).to.be.true;
            chai.expect(user.id).to.equal(stubValue.id);
            chai.expect(user.userName).to.equal(stubValue.userName);
            chai.expect(user.phoneNumber).to.equal(stubValue.phoneNumber);
            chai.expect(user.address).to.equal(stubValue.address);
            chai.expect(user.employeeId).to.equal(stubValue.employeeId);
            chai.expect(user.userTypeId).to.equal(stubValue.userTypeId);
        });
        var trans = {
            commit: function() {},
            rollback: function() {}
        };
        it("should throw error when given an invalid id", async function () {
            const stub = sinon.stub(User, "findOne").returns(null);
            const transStub = sinon.stub(common, "getTransaction").returns(trans);
            sinon.stub(trans, "rollback").value(function(){});
            await common.getTransaction();
            chai.expect(transStub.calledOnce).to.be.true;
            await chai.expect(UserService.updateUser(stubValue)).to.be.rejectedWith('User not found')
            stub.reset();
        });
    });

    describe("getAllUsers service", function () {
        it("should retrieve all users", async function () {
            const stub = sinon.stub(User, "findAndCountAll").returns(getAllUsersStub)
            const user: any = await UserService.getAllUsers()
            const userData = user.rows[0];
            chai.expect(stub.calledOnce).to.be.true;
            chai.expect(user.count).to.be.equal(2)
            chai.expect(userData.id).to.equal(stubValue.id);
            chai.expect(userData.userName).to.equal(stubValue.userName);
        });
        
    });

    describe("createUser service", function () {
        it("should create an user", async function () {
            const stub = sinon.stub(User, "create").returns(stubValue)
            const user: any = await UserService.createUser(createUserStub)
            chai.expect(stub.calledOnce).to.be.true;
            chai.expect(user.id).to.equal(stubValue.id);
            chai.expect(user.userName).to.equal(stubValue.userName);
        });

        it("should throw error when user is not created", async function () {
            const stub = sinon.stub(User, "create").returns(null)
            await chai.expect(UserService.createUser(createUserStub)).to.be.rejectedWith('Error while creating user')
            chai.expect(stub.calledOnce).to.be.true;
        });
        
    });

});