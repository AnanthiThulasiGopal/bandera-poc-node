CREATE SCHEMA IF NOT EXISTS `bandera_poc` DEFAULT CHARACTER SET latin1 ;
USE `bandera_poc` ;

CREATE TABLE IF NOT EXISTS `bandera_poc`.`userTypes` (
 `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `type` TEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


INSERT INTO `bandera_poc`.`userTypes` (`type`) VALUES ('admin');
INSERT INTO `bandera_poc`.`userTypes` (`type`) VALUES ('manager');
INSERT INTO `bandera_poc`.`userTypes` (`type`) VALUES ('employee');

CREATE TABLE IF NOT EXISTS `bandera_poc`.`users` (
 `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `userName` TEXT NULL,
  `phoneNumber` TEXT NOT NULL,
  `address` TEXT NULL,
  `employeeId` INT NOT NULL,
  `userTypeId` BIGINT(20) NULL,
  `isDeleted` TINYINT(4) NOT NULL DEFAULT 0,
  `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `users_idx0` (`employeeId`),
  KEY `users_idx1` (`userTypeId`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`userTypeId`) REFERENCES `userTypes` (`id`))
ENGINE = InnoDB;

INSERT INTO `bandera_poc`.`users` (`userName`, `phoneNumber`, `address`, `employeeId`, `userTypeId`) VALUES ('Ananthi', '8220074438', 'Madurai', '100', 1);
INSERT INTO `bandera_poc`.`users` (`userName`, `phoneNumber`, `address`, `employeeId`, `userTypeId`) VALUES ('Renesha', '4578920485', 'Chennai', '90', 2);
INSERT INTO `bandera_poc`.`users` (`userName`, `phoneNumber`, `address`, `employeeId`, `userTypeId`) VALUES ('Sapna', '9034586743', 'Erode', '110', 3);