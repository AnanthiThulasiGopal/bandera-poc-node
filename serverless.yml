service: bandera-poc-node

custom:
  Region: us-east-1

plugins:
  - serverless-offline
  - serverless-webpack

provider:
  name: aws
  runtime: nodejs12.x
  region: us-east-1
  iamManagedPolicies:
    - arn:aws:iam::aws:policy/service-role/AWSLambdaRole
  vpc:
    securityGroupIds:
      - !Ref BanderaVPCLambdaSecurityGroup
    subnetIds:
      - !Ref BanderaLambdaPrivateSubnet
      - !Ref BanderaLambdaPrivateSubnetAdditional
  tags:
    businessunit: app-dev
    createdby: ananthi.tg@coda.global
    owner: ananthi.tg@coda.global
  environment:
    DB_HOST: bb1dnf8mate5ahs.ctlzupcrhjz4.us-east-1.rds.amazonaws.com
    DB_PORT: 3306
    DATABASE: bandera_poc
    DB_USERNAME: !Ref MasterUsername
    DB_PASSWORD: !Ref MasterUserPassword
functions:
  getAllUsers:
    handler: src/api/user.getAllUsers
    events:
      - http:
          path: api/users
          cors: true
          method: get
    tags:
      Name: ban-dev-getAllUsers
      purpose: Get all users api
  getUserById:
    handler: src/api/user.getUserById
    events:
      - http:
          path: api/user/{id}
          cors: true
          method: get
    tags:
      Name: ban-dev-getUserById
      purpose: Get user by id api

  create:
    handler: src/api/user.create
    events:
      - http:
          path: api/user
          cors: true
          method: post
    tags:
      Name: ban-dev-create
      purpose: Create users api
  updateUser:
    handler: src/api/user.updateUser
    events:
      - http:
          path: api/user
          cors: true
          method: put
    tags:
      Name: ban-dev-updateUser
      purpose: Update user api

  deleteUser:
    handler: src/api/user.deleteUser
    events:
      - http:
          path: api/user/{id}
          cors: true
          method: delete
    tags:
      Name: ban-dev-deleteUser
      purpose: Delete user api

resources:
  Parameters:
    MasterUsername:
      Type: String
      Default: bandera_admin
    MasterUserPassword:
      Type: String
      NoEcho: true
      Default: testpass
    BastionImageId:
      Type: "AWS::SSM::Parameter::Value<AWS::EC2::Image::Id>"
      Default: /aws/service/ami-amazon-linux-latest/amzn-ami-hvm-x86_64-ebs
    VPCId:
      Type: String
      Default: vpc-04a546ef213cd272a
    BanderaPublicSubnet:
      Type: String
      Default: subnet-0f450384d21da9a81
    BanderaRDSPrivateSubnet:
      Type: String
      Default: subnet-0e06a9bed0f67dabf
    BanderaLambdaPrivateSubnet:
      Type: String
      Default: subnet-06943e8825a9b10b9
    BanderaLambdaPrivateSubnetAdditional:
      Type: String
      Default: subnet-01a7bcebe69720250

  Resources:

    BanderaVPCLambdaSecurityGroup:
      Type: "AWS::EC2::SecurityGroup"
      Properties:
        GroupName: Bandera-Lambda-SG
        GroupDescription: EC2 Security Group for instances launched in the VPC by Lambda
        VpcId: !Ref VPCId
        Tags:
          - Key: Name
            Value: ban-dev-lambda-sqp
          - Key: purpose
            Value: Bandera VPC Lambda Security Group

    BastionSecurityGroup:
      Type: "AWS::EC2::SecurityGroup"
      Properties:
        GroupDescription: Bastion Security Group
        SecurityGroupEgress:
          - CidrIp: 0.0.0.0/0
            FromPort: -1
            ToPort: -1
            IpProtocol: "-1"
        SecurityGroupIngress:
          - IpProtocol: tcp
            CidrIp: 0.0.0.0/0
            FromPort: 22
            ToPort: 22
          - IpProtocol: tcp
            CidrIp: 0.0.0.0/0
            FromPort: 3306
            ToPort: 3306
        VpcId: !Ref VPCId
        Tags:
          - Key: Name
            Value: ban-dev-bastion-sqp
          - Key: purpose
            Value: Bastion Security Group

    Bastion:
      Type: "AWS::EC2::Instance"
      Properties:
        DisableApiTermination: false
        ImageId: !Ref BastionImageId
        InstanceType: t2.nano
        KeyName: banderapoc
        Monitoring: false
        SubnetId: !Ref BanderaPublicSubnet
        SecurityGroupIds:
          - !Ref BastionSecurityGroup
        Tags:
          - Key: Name
            Value: ban-dev-bastion
          - Key: purpose
            Value: Bastion Server

    BanderaRDSProxySecurityGroup:
      Type: "AWS::EC2::SecurityGroup"
      Properties:
        GroupDescription: Allow inbound connections from bastion server and lambda
        VpcId: !Ref VPCId
        SecurityGroupIngress:
          - IpProtocol: tcp
            SourceSecurityGroupId: !Ref BastionSecurityGroup
            FromPort: 3306
            ToPort: 3306
          - IpProtocol: tcp
            SourceSecurityGroupId: !Ref BanderaVPCLambdaSecurityGroup
            FromPort: 3306
            ToPort: 3306
        Tags:
          - Key: Name
            Value: ban-dev-rds-proxy-sqp
          - Key: purpose
            Value: Bandera RDS Proxy Security Group

    BanderaRDSSecurityGroup:
      Type: "AWS::EC2::SecurityGroup"
      Properties:
        GroupDescription: Allow inbound connections from RDS proxy
        VpcId: !Ref VPCId
        SecurityGroupIngress:
          - IpProtocol: tcp
            SourceSecurityGroupId: !Ref BanderaRDSProxySecurityGroup
            FromPort: 3306
            ToPort: 3306
        Tags:
          - Key: Name
            Value: ban-dev-rds-sqp
          - Key: purpose
            Value: Bandera RDS Proxy Security Group

    DBSubnetGroup:
      Type: AWS::RDS::DBSubnetGroup
      Properties:
        DBSubnetGroupDescription: "RDS required subnets in different availability zones"
        SubnetIds:
          - !Ref BanderaRDSPrivateSubnet
          - !Ref BanderaLambdaPrivateSubnet
          - !Ref BanderaLambdaPrivateSubnetAdditional
        Tags:
          - Key: Name
            Value: ban-dev-db-subnet
          - Key: purpose
            Value: Bandera DB Subnet Group

    BanderaDBInstance:
      Type: AWS::RDS::DBInstance
      Properties:
        AllowMajorVersionUpgrade: true
        DBInstanceClass: db.t2.micro
        AllocatedStorage: 200
        DBSubnetGroupName: !Ref DBSubnetGroup
        VPCSecurityGroups:
          - !Ref BanderaRDSSecurityGroup
        Engine: MySQL
        EngineVersion: 5.7.28
        MasterUsername: !Ref MasterUsername
        MasterUserPassword: !Ref MasterUserPassword
        Tags:
          - Key: Name
            Value: ban-dev-rds
          - Key: purpose
            Value: Bandera DB Instance
      DeletionPolicy: Delete
  Outputs:
    RDSHost:
      Value: !GetAtt
        - BanderaDBInstance
        - Endpoint.Address
